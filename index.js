const electron = require('electron')
const {
    app,
    BrowserWindow
} = require('electron')
const path = require('path')
const url = require('url')
const Menu = electron.Menu

function createWindow() {
    // Create the browser window.
    win = new BrowserWindow({
        width: 528,
        height: 258,
        resizable: false,
    })

    // and load the index.html of the app.
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))
}

app.on('ready', function(){
    createWindow();

    const template = [
        {
            label: 'About',
            submenu: [
                {
                    label: 'About',
                    click: function() {
                        const {dialog} = require('electron')
                        dialog.showMessageBox({
                            type: 'none',
                            title: 'About',
                            message: 'Trình phát nhạc đơn giản\n\nNguyễn Quang Huy',
                            buttons: ['Ok']
                        })    
                    }
                },
                {role: 'close'},
            ]
        }
    ]

    const menu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(menu)
    // Menu.buildFromTemplate()
})
app.on('window-all-closed', () => {
  app.quit()
})
// npm run build --overwrite
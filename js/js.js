window.onload = function(){
    
}
function abcd() {
    // https://github.com/aadsm/JavaScript-ID3-Reader
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();
    reader.onloadend = function(){
        document.getElementById("audioaudio").src = reader.result;
        document.getElementById("btnPlay").disabled = false;
        document.getElementById("seekbar").disabled = false;

        ID3.loadTags("filename.mp3", function() {
            var tags = ID3.getAllTags("filename.mp3");
            // alert(tags.title + " - " + tags.track);
            document.getElementById("bh").value = tags.title;
            document.getElementById("cs").value = tags.artist;
            document.getElementById("ab").value = tags.album;
        }, {
            dataReader: ID3.FileAPIReader(file)
        });
    }
    if(file) {
        reader.readAsDataURL(file);
    }
}
$(document).ready(function () {
    $("[data-toggle=popover]").popover({
        html: true,
        content: function () {
            var content = $(this).attr("data-popover-content");
            return $(content).children(".popover-body").html();
        },
        title: function () {
            var title = $(this).attr("data-popover-content");
            return $(title).children(".popover-heading").html();
        }
    });
    $('[data-toggle="tooltip"]').tooltip();

    //animate
    $('.dropdown').on('show.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeIn(200);
    });
    $('.dropdown').on('hide.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeOut(200);
    });
    
    // btnPlay.innerHTML = "<i class='material-icons'>play_arrow</i>";\

});